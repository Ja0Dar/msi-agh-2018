from hero_journey import *
from hero_journey.consts import walk_time, wood_chopping_time, wood_price, sword_price, prize_for_monster, \
    monster_killing_time


def walk(state, hero, loc_a, loc_b):
    if state.loc[hero] == loc_a:
        state.loc[hero] = loc_b
        state.time[hero] -= walk_time[(loc_a, loc_b)]
        return state
    else:
        return False


def chop_wood(state, hero):
    if state.loc[hero] == forrest:
        state.wood[hero] += 1
        state.time[hero] -= wood_chopping_time
        return state
    else:
        return False


def buy_sword(state, hero):
    if state.loc[hero] == market and state.money[hero] >= sword_price:
        state.money[hero] -= sword_price
        state.has_sword[hero] = True
        return state
    else:
        return False


def kill_monster(state, hero):
    if state.loc[hero] == dungeon and state.time[hero] >= monster_killing_time:
        state.has_trophy[hero] = True
        state.time[hero] -= monster_killing_time
        state.has_sword[hero] = False  # hard fight, broken sword
        return state
    else:
        return False


def sell_wood(state, hero):
    if state.loc[hero] == sawmil and state.wood[hero] > 0 and state.time[hero] >= 0:
        state.wood[hero] -= 1
        state.money[hero] += wood_price
        return state
    else:
        return False


def redeem_reward_for_monster(state, hero):
    if state.loc[hero] == castle and state.has_trophy[hero] and state.time[hero] >= 0:
        state.money[hero] += prize_for_monster
        state.has_trophy[hero] = False
        return state
    else:
        return False


def wait_for_miracle(state, hero):
    if state.time[hero] >= miracle_time:
        state.time[hero] -= miracle_time
        state.money[hero] += miracle_money
        return state
    else:
        return False
