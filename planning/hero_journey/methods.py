from hero_journey import *


def earn_money_chopping_wood(state, hero, balance):
    required_time = walk_time[(home, forrest)] + wood_chopping_time + walk_time[(forrest, sawmil)]
    if state.money[hero] + wood_price >= balance \
            and state.time[hero] >= required_time:
        return [
            ('walk', hero, home, forrest),
            ('chop_wood', hero),
            ('walk', hero, forrest, sawmil),
            ('sell_wood', hero)]
    else:
        return False


def earn_money_killing_monster(state, hero, balance):
    required_time = walk_time[(home, market)] \
                    + walk_time[(market, dungeon)] \
                    + monster_killing_time + walk_time[(dungeon, castle)]

    if state.money[hero] >= sword_price \
            and state.time[hero] >= required_time \
            and state.money[hero] - sword_price + prize_for_monster >= balance:
        return [
            ('walk', hero, home, market),
            ('buy_sword', hero),
            ('walk', hero, market, dungeon),
            ('kill_monster', hero),
            ('walk', hero, dungeon, castle),
            ('redeem_reward_for_monster', hero)
        ]
    else:
        return False


def earn_money_by_miracle(state, hero, balance):
    if state.time[hero] >= miracle_time and state.money[hero] + miracle_money >= balance:
        return [('wait_for_miracle', hero)]
    else:
        return False
