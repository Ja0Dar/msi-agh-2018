home = "home"
sawmil = "sawmil"
castle = "castle"
forrest = "forrest"
dungeon = "dungeon"
market = "market"


def _with_reversed_keys(my_dict):
    return dict(
        list({(places[1], places[0]): time for places, time in my_dict.items()}.items()) + list(my_dict.items()))


_walk_time = {
    (home, sawmil): 3,
    (home, castle): 3,
    (home, forrest): 3,
    (home, dungeon): 3,
    (home, market): 3,
    (sawmil, castle): 3,
    (sawmil, forrest): 3,
    (sawmil, dungeon): 3,
    (sawmil, market): 3,
    (castle, forrest): 3,
    (castle, dungeon): 3,
    (castle, market): 3,
    (forrest, dungeon): 3,
    (forrest, market): 3,
    (dungeon, market): 3
}

walk_time = _with_reversed_keys(_walk_time)

wood_chopping_time = 3
monster_killing_time = 1
wood_price = 10
sword_price = 5
prize_for_monster = 40
miracle_time = 100
miracle_money = 100
