from hero_journey.operators import *
from hero_journey.methods import *

import pyhop as hop

if __name__ == '__main__':
    hop.declare_operators(walk, chop_wood, buy_sword, kill_monster, sell_wood, redeem_reward_for_monster,
                          wait_for_miracle
                          )
    hop.print_operators()

    hop.declare_methods('earn', earn_money_chopping_wood, earn_money_killing_monster, earn_money_by_miracle)

    hero_state = hop.State("hero")
    hero_state.loc = {'me': 'home'}
    hero_state.money = {'me': 5}
    hero_state.has_sword = {'me': False}
    hero_state.has_trophy = {'me': False}
    hero_state.has_house = {'me': False}
    hero_state.wood = {'me': 0}
    hero_state.time = {'me': 50}

    print("Earn by killing monster")
    hop.pyhop(hero_state, [('earn', 'me', 40)], verbose=1)

    print("Earn by chopping wood")
    hero_state.money = {'me': 0}
    hop.pyhop(hero_state, [('earn', 'me', 10)], verbose=1)

    print("Wait for miracle")
    hero_state.time = {'me': 100}
    hop.pyhop(hero_state, [('earn', 'me', 90)], verbose=1)
