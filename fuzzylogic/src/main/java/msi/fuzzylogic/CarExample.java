package msi.fuzzylogic;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.rule.FuzzyRuleSet;

import java.util.Arrays;

public class CarExample {

    public static void main(String[] args) throws Exception {
        try {
            String fileName = args[0];
            int distance = Integer.parseInt(args[1]);
            int slipperness = Integer.parseInt(args[2]);
            int speed = Integer.parseInt(args[3]);
            int kidsActivity= Integer.parseInt(args[4]);
            FIS fis = FIS.load(fileName, false);


            System.out.println(String.format("input: distance: %d, slipperness: %d, speed: %d, kidsActivity %d", distance,slipperness,speed,kidsActivity));

//wyswietl wykresy funkcji fuzyfikacji i defuzyfikacji
            FuzzyRuleSet fuzzyRuleSet = fis.getFuzzyRuleSet();
            fuzzyRuleSet.chart();

//zadaj wartosci wejsciowe
            fuzzyRuleSet.setVariable("distance", distance);
            fuzzyRuleSet.setVariable("slipperness", slipperness);
            fuzzyRuleSet.setVariable("speed", speed);
            fuzzyRuleSet.setVariable("kids_activity", kidsActivity);
//logika sterownika
            fuzzyRuleSet.evaluate();

//graficzna prezentacja wyjscia
            fuzzyRuleSet.getVariable("speed_change").chartDefuzzifier(true);

//System.out.println(fuzzyRuleSet);

        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Niepoprawna liczba parametrow. Przyklad: java FuzzyExample " +
                    "string<plik_fcl> " +
                    "int<distance (0-30)> " +
                    "int<sliperness(0-20)> " +
                    "int<speed(20-60)>  " +
                    "int<kids activity(0-5)>");
        } catch (NumberFormatException ex) {
            System.out.println("Niepoprawny parametr. Przyklad: java FuzzyExample string<plik_fcl> int<poziom natezenia> int<pora dnia>");
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

}