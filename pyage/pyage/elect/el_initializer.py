from pyage.core.operator import Operator
from pyage.elect.el_genotype import Votes


class ElectInitializer(Operator):
    def __init__(self, dims=3, size=100, lowerbound=0.0, upperbound=1.0):
        super(Votes, self).__init__(Votes)
        self.size = size
        self.lowerbound = lowerbound
        self.upperbound = upperbound
        self.dims = dims

    def process(self, population):
        for i in range(self.size):
            population.append(Votes([self.__randomize() for _ in range(self.dims)]))

    def __randomize(self):
        return uniform(self.lowerbound, self.upperbound)
