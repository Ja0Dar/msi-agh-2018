from functional import seq


def read_dimasc_cnf(filename):
    # https://stackoverflow.com/a/30181011
    in_data = seq(open(filename).readlines()).filter(lambda line_: "%" not in line_).map(lambda s: s.replace("\n", ""))
    cnf = list()
    cnf.append(list())
    maxvar = 0

    for line in in_data:
        tokens = line.split()
        if len(tokens) != 0 and tokens[0] not in ("p", "c"):
            for tok in tokens:
                lit = int(tok)
                maxvar = max(maxvar, abs(lit))
                if lit == 0:
                    cnf.append(list())
                else:
                    cnf[-1].append(lit)

    assert len(cnf[-1]) == 0
    cnf.pop()

    return seq(cnf).filter(lambda l: len(l) != 0), maxvar
