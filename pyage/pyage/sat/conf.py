import logging
import sys

from functional import seq

from cnf_reader import read_dimasc_cnf
from pyage.core import address
from pyage.core.agent.agent import Agent, generate_agents
from pyage.core.agent.aggregate import AggregateAgent
from pyage.core.emas import EmasService
from pyage.core.locator import GridLocator
from pyage.core.migration import ParentMigration
from pyage.core.stats.gnuplot import StepStatistics
from pyage.core.stop_condition import StepLimitStopCondition
from pyage.elect.el_init import root_agents_factory
from pyage.elect.naming_service import NamingService
from pyage.sat.domain.clauses import formulaFromTuples
from pyage.sat.sat_operators import CNFEvaluation, CNFRandomFlipMutation, CNFCrossover, CNFRandomSwapMutation
from pyage.solutions.evolution.selection import TournamentSelection
from sat_init import get_initial_marking, EmasInitializer, CNFInitializer

logger = logging.getLogger(__name__)
print(sys.argv)
mode = sys.argv[3]
filename = sys.argv[4]
mutation_strategy = (sys.argv[5])
mutation_probability = float(sys.argv[6])

choosen_mutation = CNFRandomFlipMutation(mutation_probability) if (
        mutation_strategy == "flip") else CNFRandomSwapMutation(mutation_probability)

cnf, number_of_variables = read_dimasc_cnf(filename)
formula = formulaFromTuples(cnf)

initial_marking = get_initial_marking(number_of_variables)
logger.info("Initial marking:\n%s", seq(initial_marking).map(lambda x: str(x)).reduce(lambda x, y: x + ", " + y))

evolution = mode != "emas"

# EMAS-settings
agents_count = 1 if evolution else 6
logger.debug("Agents: %s ", agents_count)

agents = root_agents_factory(agents_count, AggregateAgent)
agg_size = 60

size = 50

variables = get_initial_marking(number_of_variables)

aggregated_agents = EmasInitializer(variables, formula, size=agg_size, energy=40)

emas = EmasService

minimal_energy = lambda: 10
reproduction_minimum = lambda: 100
migration_minimum = lambda: 120
newborn_energy = lambda: 100
transferred_energy = lambda: 40

evaluation = lambda: CNFEvaluation()
crossover = lambda: CNFCrossover(size)
mutation = lambda: choosen_mutation


def simple_cost_func(x): return abs(x) * 10


stop_condition = lambda: StepLimitStopCondition(5000)
address_provider = address.SequenceAddressProvider

migration = ParentMigration
locator = GridLocator

stats = lambda: StepStatistics('fitness_%s_pyage.txt' % __name__, 30)

naming_service = lambda: NamingService(starting_number=1)
