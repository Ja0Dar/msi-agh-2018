import random

from pyage.core.emas import EmasAgent
from pyage.core.inject import Inject
from pyage.core.operator import Operator
from pyage.sat.sat_genotype import CNFGenotype


def get_initial_marking(number_of_variables):
    return [random.choice((True, False)) for i in range(number_of_variables)]


class EmasInitializer:

    def __init__(self, variables, formula, energy, size):
        """

        :type energy: int
        :type size: int
        :type formula: Formula
        :type variables: list[bool]
        """
        self.variables = variables
        self.formula = formula
        self.energy = energy
        self.size = size

    @Inject("naming_service")
    def __call__(self):
        agents = {}
        for i in range(self.size):  ## todo - maybe variables shouldnt be static?
            agent = EmasAgent(CNFGenotype(get_initial_marking(len(self.variables)), self.formula), self.energy,
                              self.naming_service.get_next_agent())
            agents[agent.get_address()] = agent
        return agents


def root_agents_factory(count, type):
    def factory():
        agents = {}
        for i in range(count):
            agent = type('R' + str(i))
            agents[agent.get_address()] = agent
        return agents

    return factory


class CNFInitializer(Operator):

    def __init__(self, popul_size, variable_nr, formula, seed):
        """

        :type formula: Formula
        :type variable_nr: int
        """
        super(CNFInitializer, self).__init__(CNFGenotype)
        self.variable_nr = variable_nr
        random.seed(seed)
        self.formula = formula
        self.popul_size = popul_size

        self.variables = [random.choice([True, False]) for _ in range(self.variable_nr)]
        self.population = [CNFGenotype(self.variables, self.formula) for _ in range(self.popul_size)]

    def __call__(self):
        return self.population

    def process(self, population):
        for i in range(self.popul_size):
            population.append(self.population[i])
