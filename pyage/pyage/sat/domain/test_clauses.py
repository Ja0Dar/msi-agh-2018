from unittest import TestCase
from functional import seq
from clauses import Clause, Formula


class TestClause(TestCase):
    def test_evaluate(self):
        variables = [True, False, True, False, True, False]

        conds = seq((1, 2), (3, 4), (5, 6)).map(lambda tup: Clause(tup).evaluate(variables)).to_list()
        self.assertEquals(conds, [True, True, True])

    def test_evaluate2(self):
        variables = [False, False, True, False, True, True]
        conds = seq((1, 2, 3), (1, 2), (5, 6)).map(lambda tup: Clause(tup).evaluate(variables)).to_list()
        self.assertEquals(conds, [True, False, True])


class TestFormula(TestClause):
    def test_true(self):
        clauses = [Clause(1) for _ in range(3)]
        res = Formula(clauses).evaluate([True])
        self.assertTrue(res)

    def test_true2(self):
        clauses = [Clause(1) for _ in range(3)]
        clauses[1] = Clause(2)
        res = Formula(clauses).evaluate([True, False])
        self.assertFalse(res)
