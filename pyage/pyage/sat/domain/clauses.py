import operator
from functional import seq


class Symbol:
    def __init__(self, symbol_int):
        self.negated = symbol_int < 0

        if self.negated:
            symbol_int = abs(symbol_int)

        self.index = symbol_int - 1  # variables are indexed from 1 when created, and indexed from 0 in arrays

    def evaluate(self, variables):
        if self.negated:
            return not variables[self.index]
        return variables[self.index]


class Clause:
    def __init__(self, symbols):
        """

        :type symbols: list[int]
        """
        symbols=seq(symbols)
        assert 0 not in symbols

        self.symbol = symbols.map(lambda s: Symbol(s))

    def evaluate(self, variables):
        """

        :type variables: list[bool]
        """
        return self.symbol.map(lambda sym: sym.evaluate(variables)).reduce(operator.or_)


class Formula:
    def __init__(self, clauses):
        """

        :type clauses: list[Clause]
        """
        self.clauses = seq(clauses)

    def evaluate(self, variables):
        """

        :type variables: list[bool]
        """
        self.clauses.map(lambda clause: clause.evaluate(variables)).reduce(operator.and_)

    def count_correct_clauses(self, variables):
        """

        :type variables: Lis[bool]
        """
        return self.clauses.sum(projection=lambda clause: int(clause.evaluate(variables)))

    def size(self):
        return self.clauses.size()

    def __len__(self):
        return self.size()


def formulaFromTuples(cnf):
    """

    :type cnf: list[tuple[int]]
    """
    clauses = seq(cnf).map(lambda iter: Clause(iter))
    return Formula(clauses.to_list())

