class CNFGenotype(object):
    def __init__(self, variables, formula):
        self.formula = formula
        self.fitness = None
        self.variables = variables

    def __str__(self):
        return "{0}\nfitness: {1}".format(",".join(map(str, self.variables)), self.fitness)
