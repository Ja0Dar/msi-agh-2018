import logging

from pyage.core.operator import Operator
from pyage.sat.sat_genotype import CNFGenotype
import random

logger = logging.getLogger(__name__)


class CNFEvaluation(Operator):
    def __init__(self):
        super(CNFEvaluation, self).__init__(CNFGenotype)

    def process(self, population):
        for genotype in population:
            genotype.fitness = self.__calculate_fitness(genotype)

    def __calculate_fitness(self, genotype):
        """

        :type genotype: CNFGenotype
        """
        return genotype.formula.count_correct_clauses(genotype.variables)


class AbstractCrossover(Operator):
    def __init__(self, type=CNFGenotype, size=20):
        super(AbstractCrossover, self).__init__(type)
        self.__size = size

    def process(self, population):
        """

        :type population: list[CNFGenotype]
        """
        parents = list(population)
        for i in range(len(population), self.__size):
            p1, p2 = random.sample(parents, 2)
            genotype = self.cross(p1, p2)
            population.append(genotype)


class CNFCrossover(AbstractCrossover):
    def __init__(self, size):
        super(CNFCrossover, self).__init__(CNFGenotype, size)

    def cross(self, p1, p2):
        """

        :type p1: CNFGenotype
        :type p2: CNFGenotype
        """
        logger.debug("Crossing: " + str(p1) + " and " + str(p2))

        parent1 = p1.variables[:]
        parent2 = p2.variables[:]

        index1 = random.randrange(0, len(parent1))
        index2 = random.randrange(0, len(parent1))

        if index1 > index2:
            index1, index2 = index2, index1

        genlist = parent2[:index1] + parent1[index1:index2] + parent2[index2:]

        genotype = CNFGenotype(genlist, p1.formula)

        logger.debug("Crossed genotype: " + str(genotype))

        return genotype


class AbstractMutation(Operator):
    def __init__(self, type=CNFGenotype, probability=0.5):
        super(AbstractMutation, self).__init__(type)
        self.probability = probability

    def process(self, population):
        for genotype in population:
            if random.random() < self.probability:
                self.mutate(genotype)


# does this mutation even work? above result is ignored
class CNFRandomFlipMutation(AbstractMutation):
    def __init__(self, probability):
        """

        :type probability: float
        """
        super(CNFRandomFlipMutation, self).__init__(CNFGenotype, probability)

    def mutate(self, genotype):
        """

        :type genotype: CNFGenotype
        """
        logger.debug("Mutating (rand flip) genotype: " + str(genotype))

        variables = genotype.variables
        i1 = random.randint(0, len(variables) - 1)

        variables[i1] = not variables[i1]

        gen = CNFGenotype(variables, genotype.formula)

        logger.debug("Mutated (rand flip) genotype: " + str(gen))

        return gen


class CNFRandomSwapMutation(AbstractMutation):
    def __init__(self, probability):
        """

        :type probability: float
        """
        super(CNFRandomSwapMutation, self).__init__(CNFGenotype, probability)

    def mutate(self, genotype):
        """

        :type genotype: CNFGenotype
        """
        logger.debug("Mutating (rand flip) genotype: " + str(genotype))

        variables = genotype.variables  # it should mutate? so no copying here
        i1 = random.randint(0, len(variables) - 1)
        i2 = random.randint(0, len(variables) - 1)

        variables[i1], variables[i2] = variables[i2], variables[i1]

        gen = CNFGenotype(variables, genotype.formula)

        logger.debug("Mutated (rand swap) genotype: " + str(gen))

        return gen
