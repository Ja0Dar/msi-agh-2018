import os

if __name__ == '__main__':
    path = "python -m pyage.core.bootstrap pyage.sat.conf ERROR"

    filename = "problems/CBS_k3_n100_m403_b10_0.cnf"
    # filename = "problems/uf20-01.cnf"

    modes = ["emas", "evol"]


    fn_suffix = filename.split(".")[0][-5:]
    mutations = [0.01, 0.1, 0.3, 0.6]
    mutation_strategies = ["swap", "flip"]
    for mode in modes:
        for mutation_prob in mutations:
            for mutation_strategy in mutation_strategies:
                os.system("{} {} {} {} {}".format(path, mode, filename, mutation_strategy, mutation_prob))

                new_name = "{}_{}_{}_{}.txt".format(fn_suffix, mode, mutation_strategy,
                                                    str(mutation_prob).replace(".", "-"))
                print("Finished {}".format(new_name))
                os.system("mv fitness_pyage.sat.conf_pyage.txt {}".format(new_name))
